<?php
/**
 * @author   	cg@089webdesign.de
 */
 

defined('_JEXEC') or die;
//include system
include_once(JPATH_ROOT . "/templates/" . $this->template . '/lib/system.php');
//include template Functions CG
include_once(JPATH_ROOT . "/templates/" . $this->template . '/template_functions.php');
//
?>
<!DOCTYPE html>
<html lang="de-de">
<head>
	<?php 
	// including head
	include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/head.php');
	?>	
</head>

<body id="body" class="site <?php echo $option
	. ' view-' . $view
	. ($layout ? ' layout-' . $layout : ' no-layout')
	. ($task ? ' task-' . $task : ' no-task')
	. ($itemid ? ' itemid-' . $itemid : '')
	. $body_class . $detectAgent; print ($clientMobile) ? "mobile " : " ";
?>">
<?php 
/*FB newsfeedPage */
	if ($currentMenuID == 125)
	print '<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/de_DE/sdk.js#xfbml=1&version=v2.5&appId=459204684217310";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, \'script\', \'facebook-jssdk\'));</script>';
	//https://developers.facebook.com/docs/plugins/page-plugin 
?>

	<!-- Body -->
	<div class="animsition">

		<!-- <div id="kontakt-wrapper"><div id="kontakt" class="visible-desktop"><a href="/kontakt.html"></a></div></div> -->

			<div class="site_wrapper">
				<div class="visible-desktop ribbon-wrap right-edge fork lblue">
					<span>
						<a class="typo" href="/kontakt-personaltraining-ernaehrungsberatung-gilching.html" title="Kontaktformular">Frag mich einfach!</a>
						<a class="env_open" href="/kontakt-personaltraining-ernaehrungsberatung-gilching.html" title="Kontaktformular"><img src="/images/env_open.png" alt="Nachricht senden Bild" /></a>
					</span>
				</div>				
				<?php			
//preprint($useragent);
				// including slider
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/slider.php');

				// including header designbedingt nich benötigt
				//include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/header.php');			

				// including header
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/menu.php');

				// including top
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/breadcrumbs.php');				
								
				// including top
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/top.php');	
				
				// including top2
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/top2.php');	
										
				// including content
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/content.php');	
				
				// including bottom
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/bottom.php');	
				
				// including bottom2
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/bottom2.php');
				
				// including content
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/footer.php');				
				
				?>					
				
			</div>
		</div>
	
	
	<jdoc:include type="modules" name="debug" style="none" />

	<script>
	    <?php /*Varaiblen standard Init für FX
	    	var inEffect = "fade-in";
			var outEffect = "fade-out";
			var inDur = 800;
			var outDur = 700;

		<? /*Mobile Detection
			    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
			    	inEffect = "fade-in-left-sm";
			    	outEffect = "fade-out";
			    	inDur = 900;
			    	outDur = 700;
			    } */ ?>

			<? // siehe: http://git.blivesta.com/animsition/ ?>
		jQuery(document).ready(function() {
		  
		  jQuery(".animsition").animsition({
		  
		    inClass               :   "fade-in",
		    outClass              :   "fade-out",
		    inDuration            :    900,
		    outDuration           :    800,
		    linkElement           :   '.animsition-link',
		    // e.g. linkElement   :   'a:not([target="_blank"]):not([href^=#])'
		    loading               :    true,
		    loadingParentElement  :   'body', //animsition wrapper element
		    loadingClass          :   'animsition-loading',
		    unSupportCss          : [ 'animation-duration',
		                              '-webkit-animation-duration',
		                              '-o-animation-duration'
		                            ],
		    <? /*"unSupportCss" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
		    //The default setting is to disable the "animsition" in a browser that does not support "animation-duration". */ ?>
		    
		    overlay               :   false,
		    
		    overlayClass          :   'animsition-overlay-slide',
		    overlayParentElement  :   'body'
		  });
		});
	</script>
</body>
</html>
