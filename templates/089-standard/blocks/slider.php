<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
defined('_JEXEC') or die;
?>
<?php if ($this->countModules('slider') && $detect->isTablet() ) { ?>
<div class="clear-slider">
	<div class="clear-slider-wrap">		
		<jdoc:include type="modules" name="slider" style="none" />		
	</div>
</div>
<?php } else if ($this->countModules('slider_mobile') && $detect->isMobile() && !$detect->isTablet() ) { ?>
	<div class="clear-slider_mobile">
	<div class="clear-slider-wrap_mobile">		
		<jdoc:include type="modules" name="slider_mobile" style="none" />		
	</div>
</div>
<?php } else if ($this->countModules('slider') && !$detect->isMobile()) { ?>
	<div class="clear-slider">
		<div class="clear-slider-wrap">		
			<jdoc:include type="modules" name="slider" style="none" />		
		</div>
	</div>
<?php } ?>